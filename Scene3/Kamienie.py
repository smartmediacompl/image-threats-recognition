from __future__ import print_function
from __future__ import division
from skimage.measure import compare_ssim
from PIL import Image
import cv2 as cv
import numpy as np
import argparse
import os
from pathlib import Path
from matplotlib import pyplot as plt


###################################################################################################
def crop_road(im):
    # CropRoad
    width, height = im.size
    pts = np.array(
        [[0, 0], [0, 300 + height / 2], [width / 2 - 150, height / 2 - 200], [width / 2 + 200, height / 2 - 100],
         [width / 2 + 110, height], [width, height], [width, 0]], np.int32)
    pts = pts.reshape((-1, 1, 2))
    return pts


def count_white_px(src):
    arr = np.array(src)
    count = 0
    for i in range(len(arr)):
        for j in range(len(arr[i])):
            if arr[i][j] > 128:
                count += 1
    return count


def write_file(image_list):
    f_name = open("temp.txt", "w+")
    for image in image_list:
        # if image.white_px_diff > 78000 and image.white_px_edges > 18000:
        #     image.result = "Kamienie"
        # else:
        image.result = "----"

        f_name.write("{}\r\n".format(image))

    f_name.close()
    return


def equalize(img):
    hist, bins = np.histogram(img.flatten(), 256, [0, 256])
    cdf = hist.cumsum()
    cdf_m = np.ma.masked_equal(cdf, 0)
    cdf_m = (cdf_m - cdf_m.min()) * 255 / (cdf_m.max() - cdf_m.min())
    return np.ma.filled(cdf_m, 0).astype('uint8')


def no_filter(filename):
    img = cv.imread(filename, cv.IMREAD_GRAYSCALE)
    im = Image.open(filename)
    cv.fillPoly(img, [crop_road(im)], (0, 0, 0))
    # img_blurred1 = cv.GaussianBlur(img, (3, 3), 0)
    edges = cv.Canny(img, 100, 200)
    _, src = cv.threshold(edges, 230, 255, cv.THRESH_BINARY)
    return edges


def sharp_filter_clahe(filename):
    img = cv.imread(filename, cv.IMREAD_GRAYSCALE)
    im = Image.open(filename)
    img = equalize(img)[img]
    clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    cl1 = clahe.apply(img)
    cv.fillPoly(cl1, [crop_road(im)], (0, 0, 0))
    # img_blurred1 = cv.GaussianBlur(img, (3, 3), 0)
    edges = cv.Canny(cl1, 100, 200)
    _, src = cv.threshold(edges, 230, 255, cv.THRESH_BINARY)
    return edges


def sharp_filter(filename):
    img = cv.imread(filename, cv.IMREAD_GRAYSCALE)
    im = Image.open(filename)
    img = equalize(img)[img]
    cv.fillPoly(img, [crop_road(im)], (0, 0, 0))
    # img_blurred1 = cv.GaussianBlur(img, (3, 3), 0)
    edges = cv.Canny(img, 100, 200)
    _, src = cv.threshold(edges, 230, 255, cv.THRESH_BINARY)
    return edges


def clahe_filter(filename):
    img = cv.imread(filename, cv.IMREAD_GRAYSCALE)
    im = Image.open(filename)
    clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    cl1 = clahe.apply(img)
    cv.fillPoly(cl1, [crop_road(im)], (0, 0, 0))
    # img_blurred1 = cv.GaussianBlur(img, (3, 3), 0)
    edges = cv.Canny(cl1, 100, 200)
    _, src = cv.threshold(edges, 230, 255, cv.THRESH_BINARY)
    return edges


def main():
    ## [Load image]
    image_list = []
    directory = r'C:\Users\m.kulwicki\Desktop\GovTech_E1_U17\Scene3'
    os.chdir(directory)
    glob_path = Path(directory)
    file_list = [str(pp) for pp in glob_path.glob("**/*.Png")]

    pic_num = 0
    for filename in file_list:  # assuming gif
        edges_nofilter = no_filter(filename)
        edges_sharp = sharp_filter(filename)
        edges_sharp_clahe = sharp_filter_clahe(filename)
        edges_clahe = clahe_filter(filename)
        pic_num += 1
        print(os.path.split(filename)[1])
        image_list.append(FileInfo(pic_num,
                                   filename,
                                   count_white_px(edges_nofilter),
                                   count_white_px(edges_sharp),
                                   count_white_px(edges_sharp_clahe),
                                   count_white_px(edges_clahe)))

        # cv.imshow("img", img)
        # cv.imshow("img2", img2)
        # cv.waitKey()

    write_file(image_list)
    # cv.waitKey()
    return


class FileInfo:
    def __init__(self, pic_num, name, edges_nofilter, edges_sharp, edges_sharp_clahe, edges_clahe):
        self.pic_num = pic_num
        self.name = name
        self.edges_nofilter = edges_nofilter
        self.edges_sharp = edges_sharp
        self.edges_sharp_clahe = edges_sharp_clahe
        self.edges_clahe = edges_clahe

    def set_result(self, result):
        self.result = result

    def __str__(self):
        return "{} File Name: ;{};, result: ;{};, nofilter= ;{};, sharp = ;{};, sharp_clahe= ;{};, clahe= ;{};"\
            .format(self.pic_num, self.name, self.result, self.edges_nofilter, self.edges_sharp, self.edges_sharp_clahe, self.edges_clahe)

    pic_num = 0
    name = ""
    edges_nofilter = 0
    edges_sharp = 0
    edges_sharp_clahe = 0
    edges_clahe = 0
    result = ""


###################################################################################################
if __name__ == "__main__":
    main()
