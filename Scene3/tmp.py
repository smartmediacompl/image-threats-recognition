imgGrayscale = cv.cvtColor(imgOriginal, cv.COLOR_BGR2GRAY)  # convert to grayscale

imgBlurred = cv.GaussianBlur(imgGrayscale, (5, 5), 0)  # blur lite
edges = cv.Canny(imgBlurred,100,200)

retval2, src = cv.threshold(edges, 230, 255, cv.THRESH_BINARY)


if src is None:
    print('Could not open or find the image:', args.input)
    exit(0)
## [Load image]

## [Separate the image in 3 places ( B, G and R )]
bgr_planes = cv.split(src)
## [Separate the image in 3 places ( B, G and R )]

## [Establish the number of bins]
histSize = 256
## [Establish the number of bins]

## [Set the ranges ( for B,G,R) )]
histRange = (0, 256) # the upper boundary is exclusive
## [Set the ranges ( for B,G,R) )]

## [Set histogram param]
accumulate = False
## [Set histogram param]

## [Compute the histograms]
b_hist = cv.calcHist(bgr_planes, [0], None, [histSize], histRange, accumulate=accumulate)
## [Compute the histograms]

## [Draw the histograms for B, G and R]
hist_w = 512
hist_h = 400
bin_w = int(round( hist_w/histSize ))

histImage = np.zeros((hist_h, hist_w, 3), dtype=np.uint8)
## [Draw the histograms for B, G and R]

## [Normalize the result to ( 0, histImage.rows )]
cv.normalize(b_hist, b_hist, alpha=0, beta=hist_h, norm_type=cv.NORM_MINMAX)
## [Normalize the result to ( 0, histImage.rows )]
# print(int(round(b_hist[255])))
arr = np.array(src)
count = 0
for i in range(len(arr)):
    for j in range(len(arr[i])):
        if arr[i][j] > 128:
            count += 1
print(count)

## [Draw for each channel]
for i in range(1, histSize):
    cv.line(histImage, ( bin_w*(i-1), hist_h - int(round(b_hist[i-1])) ),
            ( bin_w*(i), hist_h - int(round(b_hist[i])) ),
            ( 180, 180, 180), thickness=2)
## [Draw for each channel]

## [Display]
cv.imshow('base', base)
cv.imshow('Source image', imgOriginal)

cv.imshow('edges', edges)
# cv.imshow('calcHist Demo', histImage)
cv.waitKey()
## [Display]
