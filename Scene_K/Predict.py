import sys
import cv2 as cv
from pandas import read_csv
from sklearn.externals import joblib
import re
import datetime
import Combo
import Config


def load_data_from_image(mask, filename):
    time_regex = r"\d\d\d\d-\d\d-\d\d \d\d-\d\d-\d\d"
    img = cv.imread("img/" + filename, cv.IMREAD_GRAYSCALE)

    if img.shape != mask.shape:
        print("rejected: " + filename)
        exit()

    img = cv.bitwise_and(img, mask)
    img = Combo.clahe_filter(img)

    date = re.search(time_regex, filename).group()
    time = datetime.datetime.strptime(date, '%Y-%m-%d %H-%M-%S')

    return [[int(Combo.is_night(time))]
            + Combo.FileInfo.get_statistics(Combo.slippery(img))
            + Combo.FileInfo.get_statistics(Combo.stones(img))]


def load_data_from_file(data_filename):
    names = ['name', 'slippery', 'dirty', 'night',
             'slippery_total', 'slippery_proportion', 'slippery_mean', 'slippery_deviation',
             'dirty_total', 'dirty_proportion', 'dirty_mean', 'dirty_deviation']
    dataset = read_csv(data_filename, sep=";", names=names)

    array = dataset.values
    labels = array[:, 0]
    x = array[:, 3:]

    return labels, x


def main():
    model_filename = "models/" + sys.argv[1] + ".model"
    model = joblib.load(model_filename)

    if sys.argv[2]:
        mask = Config.dict_crop[sys.argv[1]]
        labels = [sys.argv[2]]
        x = load_data_from_image(mask, sys.argv[2])
    else:
        labels, x = load_data_from_file("temp.csv")

    y = model.predict(x)
    # show the inputs and predicted outputs
    for i in range(len(x)):
        print("X=%s, Predicted=%s" % (labels[i], y[i]))


if __name__ == "__main__":
    main()
