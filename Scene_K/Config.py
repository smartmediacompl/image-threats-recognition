from __future__ import print_function
from __future__ import division
import cv2 as cv
import math
import numpy as np
import os
from pathlib import Path
import re
import datetime


###################################################################################################
dict_crop = {}
dict_recon = {}


###################################################################################################
def get_mask(shape, points):
    points_tab = points.split(",")
    t = []
    if len(points_tab) % 2 == 0:
        for i in range(0, len(points_tab), 2):
            row = [int(points_tab[i]), int(points_tab[i+1])]
            t.append(row)
    shape_tab = shape.split(",")
    if len(shape_tab) == 2:
        return generate_mask(int(shape_tab[0]), int(shape_tab[1]), t)
    return None


def generate_numpy_points(t):
    pts = np.array(t, np.int32)
    pts = pts.reshape((-1, 1, 2))
    return pts


def generate_mask(height, width, t):
    contour = generate_numpy_points(t)
    mask = np.zeros((height, width), dtype=np.uint8)
    cv.fillConvexPoly(mask, contour, 255)
    return mask


def get_recon(recon):
    recon_tab = recon.split(",")
    if len(recon_tab) == 2:
        return [int(recon_tab[0]), int(recon_tab[0])]
    return None


def main():
    directory = os.getcwd()
    glob_path = Path(directory)
    file_list = [str(pp) for pp in glob_path.glob("Config.txt")]

    for file_path in file_list:  # assuming gif
        filename = os.path.split(file_path)[1]
        f = open(filename, "r")
        content = f.readlines()
        for line in content:
            line_string_tab = line.split(";")
            if len(line_string_tab) == 4:
                mask = get_mask(line_string_tab[1], line_string_tab[2])
                if mask is not None:
                    dict_crop[line_string_tab[0]] = mask
                recon = get_recon(line_string_tab[3])
                if recon is not None:
                    dict_recon[line_string_tab[0]] = recon
    return


###################################################################################################
main()
