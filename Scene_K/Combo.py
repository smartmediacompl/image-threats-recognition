from __future__ import print_function
from __future__ import division
import cv2 as cv
import math
import numpy as np
import os
from pathlib import Path
import re
import Sunset
import datetime
import Config
import Analyze

###################################################################################################

image_list = []
test_data = {}


###################################################################################################
def count_white_px(src):
    arr = np.array(src)
    count = 0
    for i in range(len(arr)):
        for j in range(len(arr[i])):
            if arr[i][j] > 128:
                count += 1
    return count


def count_white_px2(src):
    arr = np.array(src)
    count = [0] * int((math.ceil(len(arr) / 100) * math.ceil(len(arr[0]) / 100)))
    length = math.ceil(len(arr[0]) / 100)
    for i in range(len(arr)):
        for j in range(len(arr[i])):
            if arr[i][j] > 128:
                count[int(math.floor(i / 100) * length + math.floor(j / 100))] += 1
    return count


def count_white_px_max_in_fragment(src, x, y):
    move_x = x
    move_y = y
    arr = np.array(src)
    count_max = 0
    count = 0
    size_x = len(arr) / x
    size_y = len(arr[0]) / y
    count_anal = 0

    while move_x < len(arr):
        while move_y < len(arr[0]):
            for i in range(move_x - x, move_x):
                for j in range(move_y - y, move_y):
                    if arr[i][j] > 128:
                        count += 1
            if count_max < count:
                count_max = count
            count = 0
            move_y = move_y + y
            count_anal = count_anal + 1
        move_x = move_x + x
        move_y = y

    print(count_anal)
    print(count_max)
    print(size_x)
    print(size_y)
    return count_max


def write_file(image_list):
    f_name = open("temp.txt", "w+")
    for image in image_list:
        # if image.white_px_diff > 4000:
        #    image.result = "Slisko"
        # else:
        image.result = "----"
        f_name.write("{}\n".format(image))

    f_name.close()
    return


def write_csv(image_list):
    f_name = open("temp.csv", "w+")
    for image in image_list:
        f_name.write("{}\n".format(image.to_csv()))

    f_name.close()
    return


def is_night(time):
    for sunset in Sunset.sunset_list:
        if sunset.sunrise.date() == time.date() and sunset.sunrise < time < sunset.thesaurus:
            return False
    return True


def slippery(img):
    img_blurred = cv.GaussianBlur(img, (9, 9), 0)  # blur lite
    _, src = cv.threshold(img_blurred, 250, 255, cv.THRESH_BINARY)
    return count_white_px2(src)


def stones(img):
    edges = cv.Canny(img, 200, 300)
    return count_white_px2(edges)


def clahe_filter(img):
    clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    clahe.apply(img)
    return img


# use- img = equalize(img)[img]
def equalize(img):
    hist, bins = np.histogram(img.flatten(), 256, [0, 256])
    cdf = hist.cumsum()
    cdf_m = np.ma.masked_equal(cdf, 0)
    cdf_m = (cdf_m - cdf_m.min()) * 255 / (cdf_m.max() - cdf_m.min())
    return np.ma.filled(cdf_m, 0).astype('uint8')


def init_test_data(filename):
    f = open(filename, "r")
    content = f.readlines()
    f.close()
    for line in content:
        data = line.strip().split("\t")
        wet = False
        dirty = False
        if len(data) >= 2 and data[1] == '1':
            wet = True

        if len(data) >= 3 and data[2] == '1':
            dirty = True

        test_data[data[0]] = [wet, dirty]


def main():
    regex = r"\d\d\d\d-\d\d-\d\d \d\d-\d\d-\d\d"
    init_test_data("reference.txt")

    # [Load image]
    directory = os.getcwd() + "/img"
    glob_path = Path(directory)
    file_list = [str(pp) for pp in glob_path.glob("**/*.Png")]

    pic_num = 0
    for file_path in file_list:  # assuming gif
        img = cv.imread(file_path, cv.IMREAD_GRAYSCALE)
        filename = os.path.split(file_path)[1]
        date = re.search(regex, filename).group()
        name = filename.split('_')
        if len(name) > 2 and name[0]+'_'+name[1] in Config.dict_crop:
            if img.shape != Config.dict_crop[str(name[0]+'_'+name[1])].shape:
                print(filename)
            else:
                img = cv.bitwise_and(img, Config.dict_crop[str(name[0]+'_'+name[1])])
                img = clahe_filter(img)

        time = datetime.datetime.strptime(date, '%Y-%m-%d %H-%M-%S')
        image_list.append(FileInfo(pic_num, filename, time, img))
        pic_num += 1

    #    for image in image_list:
    #        image.white_px_slisko = slippery(image.time, image.cropped_img)

    for image in image_list:
        image.white_px_slisko = slippery(image.cropped_image)
        image.white_px_gruz = stones(image.cropped_image)

    write_file(image_list)
    write_csv(image_list)
    return


class FileInfo:

    def __init__(self, pic_num, name, time, cropped_img):
        self.pic_num = pic_num
        self.name = name
        self.cropped_img = cropped_img
        self.night = is_night(time)
        self.white_px_slisko = []
        self.white_px_gruz = []
        self.time = time

    def set_result(self, result):
        self.result = result

    def to_csv(self):
        t = [self.name, test_data[self.name][0], test_data[self.name][1], self.night]\
            + self.get_statistics(self.white_px_slisko)\
            + self.get_statistics(self.white_px_gruz)
        return ';'.join(str(e) for e in t)

    @staticmethod
    def format_table(t):
        if len(t) == 0:
            return "\t\t\t"

        return "w{}\t{:>6.2f}%\ta{:>6.2f}\tr{:>6.2f}".format(
            sum(t),
            (len(t) - t.count(0)) / len(t),
            np.mean(t),
            np.std(t)
        )

    @staticmethod
    def get_statistics(t):
        if len(t) == 0:
            return [0, 0, 0, 0]
        else:
            return [sum(t), (len(t) - t.count(0)) / len(t), np.mean(t), np.std(t)]

    def __str__(self):
        test = ""
        if test_data[self.name][0] == 1:
            test = test + "s"

        if test_data[self.name][1] == 1:
            test = test + "g"

        return "{:<50}\t{}\t{}\t{}\t{}".format(
            self.name,
            self.night,
            test,
            self.format_table(self.white_px_slisko),
            self.format_table(self.white_px_gruz)
        )

    pic_num = 0
    name = ""
    result = ""


###################################################################################################
if __name__ == "__main__":
    main()
    Analyze.main()

#   mokrosc:
#           w1                       w2
#   dla 1   7000+   r>130-150
#   dla 2   10-11k  r>150
#   dla 3   55k &   r>100 & not g?
#
#   gruzowatosc:
#   tak:
#   nie:
