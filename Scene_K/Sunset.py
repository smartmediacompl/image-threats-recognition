import datetime
import os
from pathlib import Path
import re

sunset_list = []


def read_file_sunset(content):
    mount_counter = 1
    counter = 0
    for line in content:
        sunset = get_sunset_time(line)
        if sunset is None:
            return
        lp = int(sunset.day_num)
        if counter < lp:
            counter = lp
            sunset.set_month(mount_counter)
        else:
            counter = 0
            mount_counter = mount_counter + 1
            if mount_counter > 12:
                return
            sunset.set_month(mount_counter)
        sunset_list.append(sunset)
    return


def print_sunset_list():
    for data in sunset_list:
        print(data)
    return


def read_sunset():
    directory = os.getcwd()
    glob_path = Path(directory)
    file_list = [str(pp) for pp in glob_path.glob("Sunset_Glogow.txt")]

    for file_path in file_list:  # assuming gif
        filename = os.path.split(file_path)[1]
        f = open(filename, "r")
        content = f.readlines()
        read_file_sunset(content)

    return


def get_sunset_time(line):
    regex = r"\d\d:\d\d"
    data = line.split("	")

    if len(data) < 3:
        return Sunset
    s_time = re.search(regex, data[1]).group()
    t_time = re.search(regex, data[2]).group()

    return Sunset(
        data[0],
        datetime.datetime.strptime(s_time, '%H:%M').time(),
        datetime.datetime.strptime(t_time, '%H:%M').time())


class Sunset:

    def __init__(self, day_num, sunrise, thesaurus):
        self.day_num = day_num
        self.sunrise = sunrise
        self.thesaurus = thesaurus

    def set_month(self, month):
        date_sunrise = '2019:' + str(month) + ':' + str(self.day_num) + ' ' + str(self.sunrise)
        date_thesaurus = '2019:' + str(month) + ':' + str(self.day_num) + ' ' + str(self.thesaurus)
        self.sunrise = datetime.datetime.strptime(date_sunrise, '%Y:%m:%d %H:%M:%S')
        self.thesaurus = datetime.datetime.strptime(date_thesaurus, '%Y:%m:%d %H:%M:%S')

    def __str__(self):
        return "{};{}".format(self.sunrise, self.thesaurus)

    day_num = 0
    sunrise = 0
    thesaurus = 0


###################################################################################################
read_sunset()
