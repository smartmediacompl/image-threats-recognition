import re
from sklearn.externals import joblib
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import datetime
import Config
import Combo
import cv2 as cv
import numpy as np

reference_data = {}
training_data = {}


def split_reference_data():
    for d in Config.dict_recon.keys():
        reference_data[d] = {}
        for k, v in Combo.test_data.items():
            if k.startswith(d):
                reference_data[d][k] = v


def main():
    time_regex = r"\d\d\d\d-\d\d-\d\d \d\d-\d\d-\d\d"
    Combo.init_test_data("reference.txt")
    split_reference_data()

    for k, v in reference_data.items():
        training_data[k] = []
        mask = Config.dict_crop[k]
        for filename, ref in v.items():
            print "analyzing " + filename
            img = cv.imread("img/" + filename, cv.IMREAD_GRAYSCALE)

            if img.size == 0:
                print("MISSING: " + filename)
                continue

            if img.shape != mask.shape:
                print("REJECTED: " + filename)
                continue

            img = cv.bitwise_and(img, mask)
            img = Combo.clahe_filter(img)

            date = re.search(time_regex, filename).group()
            time = datetime.datetime.strptime(date, '%Y-%m-%d %H-%M-%S')

            ref = ref + [int(Combo.is_night(time))]
            ref = ref + Combo.FileInfo.get_statistics(Combo.slippery(img))
            ref = ref + Combo.FileInfo.get_statistics(Combo.stones(img))

            reference_data[k][filename] = ref
            training_data[k].append(ref)

    for key, data in training_data.items():
        print "Training " + key
        data = np.array(data)
        x = data[:, 2:]
        y = np.bitwise_or(np.array(data[:, 0], dtype=np.bool), np.array(data[:, 1], dtype=np.bool))

        x_train, x_validation, y_train, y_validation = train_test_split(x, y, test_size=0.20, random_state=1)

        model = LogisticRegression(solver='liblinear', multi_class='ovr')
        model.fit(x_train, y_train)

        filename = 'models/' + key + '.model'
        joblib.dump(model, filename)

    print "Finished"


if __name__ == '__main__':
    main()
