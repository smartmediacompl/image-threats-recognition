# visualize the data
import numpy as np
from pandas import read_csv
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.externals import joblib
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.model_selection import train_test_split, StratifiedKFold, cross_val_score
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.utils.multiclass import type_of_target


def main():
    names = ['name', 'slippery', 'dirty', 'night',
             'slippery_total', 'slippery_proportion', 'slippery_mean', 'slippery_deviation',
             'dirty_total', 'dirty_proportion', 'dirty_mean', 'dirty_deviation']

    dataset = read_csv("temp.csv", sep=";", header=None, names=names)
    dataset['night'] = dataset['night'].astype(int)
    dataset['slippery'] = dataset['slippery'].astype(bool)
    dataset['dirty'] = dataset['dirty'].astype(bool)

    # Split-out validation dataset
    array = dataset.values
    x = array[:, 3:]
    # y = array[:, 1:3]
    y = np.bitwise_or(np.array(array[:, 1], dtype=np.bool), np.array(array[:, 2], dtype=np.bool))
    # y = np.packbits(np.array(array[:, 1:3], dtype=np.bool), axis=-1)
    print y
    print type_of_target(y)

    x_train, x_validation, y_train, y_validation = train_test_split(x, y, test_size=0.20, random_state=1)

    # 0  LDA    77
    # 1  LR     99
    # 0+1   LR  81, LR 80

    # v1    KNN 82~12
    # v2    LDA 96~8    KNN 88~9
    # v3    LDA 71~18   KNN 54~14
    # v3a
    # v3b
    # v3c

    # Spot Check Algorithms
    models = [('LR', LogisticRegression(solver='liblinear', multi_class='ovr')), ('LDA', LinearDiscriminantAnalysis()),
              ('KNN', KNeighborsClassifier()), ('CART', DecisionTreeClassifier()), ('NB', GaussianNB()),
              ('SVM', SVC(gamma='auto'))]
    # evaluate each model in turn
    results = []
    names = []
    for name, model in models:
        kfold = StratifiedKFold(n_splits=10, random_state=1)
        # kfold = KFold(n_splits=10, random_state=1)
        cv_results = cross_val_score(model, x_train, y_train, cv=kfold, scoring='accuracy')
        results.append(cv_results)
        names.append(name)
        print('%s: %f (%f)' % (name, cv_results.mean(), cv_results.std()))

    exit()
    model = LogisticRegression(solver='liblinear', multi_class='ovr')
    model.fit(x_train, y_train)

    filename = 'finalized_model.sav'
    joblib.dump(model, filename)

    predictions = model.predict(x_validation)

    # Evaluate predictions
    print(accuracy_score(y_validation, predictions))
    print(confusion_matrix(y_validation, predictions))
    print(classification_report(y_validation, predictions))

# load the model from disk
    loaded_model = joblib.load(filename)
    result = loaded_model.score(x_validation, y_validation)
    print(result)


if __name__ == "__main__":
    main()
