from __future__ import print_function
from __future__ import division
from skimage.measure import compare_ssim
from PIL import Image
import cv2 as cv
import numpy as np
import argparse
import os
from pathlib import Path


###################################################################################################
def crop_road(im):
    # CropRoad
    width, height = im.size
    pts = np.array(
        [[0, 0], [0, height], [width, height], [width / 2 - 300, height / 2 - 50],
         [width / 2, height / 2 - 120], [width - 90, height / 2 + 50], [width - 300, height - 130]
            , [width, height], [width, 0]], np.int32)
    pts = pts.reshape((-1, 1, 2))
    return pts


def count_white_px(src):
    arr = np.array(src)
    count = 0
    for i in range(len(arr)):
        for j in range(len(arr[i])):
            if arr[i][j] > 128:
                count += 1
    print(count)
    return count


def write_file(image_list):
    f_name = open("temp.txt", "w+")
    for image in image_list:
        if image.white_px_diff > 20000:
            image.result = "Zalane"
        else:
            image.result = "----"

        f_name.write("{}\r\n".format(image))

    f_name.close()
    return


def equalize(cdf):
    cdf_m = np.ma.masked_equal(cdf, 0)
    cdf_m = (cdf_m - cdf_m.min()) * 255 / (cdf_m.max() - cdf_m.min())
    return np.ma.filled(cdf_m, 0).astype('uint8')


def main():
    ## [Load image]
    image_list = []
    directory = r'C:\Users\m.kulwicki\Desktop\GovTech_E1_U17\Scene1'
    os.chdir(directory)
    glob_path = Path(directory)
    file_list = [str(pp) for pp in glob_path.glob("**/*.Png")]

    pic_num = 0
    for filename in file_list:  # assuming gif
        img = cv.imread(filename, cv.IMREAD_GRAYSCALE)
        im = Image.open(filename)
        hist, bins = np.histogram(img.flatten(), 256, [0, 256])
        cdf = hist.cumsum()
        img = equalize(cdf)[img]
        cv.fillPoly(img, [crop_road(im)], (0, 0, 0))
        # img_blurred = cv.GaussianBlur(img, (5, 5), 0)  # blur lite
        edges = cv.Canny(img, 100, 200)
        _, src = cv.threshold(img, 250, 255, cv.THRESH_BINARY)
        pic_num += 1
        image_list.append(FileInfo(pic_num, filename, count_white_px(edges)))
        print(filename)
        cv.imshow("tmp", edges)
        cv.imshow("img", img)
        cv.waitKey()

    write_file(image_list)
    cv.waitKey()
    return


class FileInfo:

    def __init__(self, pic_num, name, white_px):
        self.pic_num = pic_num
        self.name = name
        self.white_px_diff = white_px

    def set_result(self, result):
        self.result = result

    def __str__(self):
        return "{} File Name: {}, result: {}, w_px= {}".format(self.pic_num, self.name, self.result, self.white_px_diff)

    pic_num = 0
    name = ""
    white_px_diff = 0
    result = ""


###################################################################################################
if __name__ == "__main__":
    main()
