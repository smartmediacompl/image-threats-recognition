from __future__ import print_function
from __future__ import division
from skimage.measure import compare_ssim
from PIL import Image
import cv2 as cv
import math
import numpy as np
import argparse
import os
from pathlib import Path
import re
import datetime

###################################################################################################
image_list = []


###################################################################################################
def crop_road(place):
    pts = np.array(place, np.int32)
    pts = pts.reshape((-1, 1, 2))
    return pts


def count_white_px(src):
    arr = np.array(src)
    count = 0
    for i in range(len(arr)):
        for j in range(len(arr[i])):
            if arr[i][j] > 128:
                count += 1
    return count


def count_white_px2(src):
    arr = np.array(src)
    count = [0] * int((math.ceil(len(arr) / 100) * math.ceil(len(arr[0]) / 100)))
    length = math.ceil(len(arr[0]) / 100)
    for i in range(len(arr)):
        for j in range(len(arr[i])):
            if arr[i][j] > 128:
                count[int(math.floor(i / 100) * length + math.floor(j / 100))] += 1
    return count


def write_file(image_list):
    f_name = open("temp.txt", "w+")
    for image in image_list:
        #if image.white_px_diff > 4000:
        #    image.result = "Slisko"
        #else:
        image.result = "----"
        f_name.write("{}\r\n".format(image))

    f_name.close()
    return


def is_night(time):
    # Wszystkie probki sa z sierpnia wiec wschod 5:15, zachod 20:23
    time_end_day = datetime.datetime.strptime('20-23-00', '%H-%M-%S').time()
    time_start_day = datetime.datetime.strptime('5-15-00', '%H-%M-%S').time()

    if time > time_end_day or time < time_start_day:
        return True
    return False


def slippery(time, file_path, img, pic_num):
    if is_night(time):
        print(time)
        img_blurred = cv.GaussianBlur(img, (5, 5), 0)  # blur lite
        _, src = cv.threshold(img_blurred, 250, 255, cv.THRESH_BINARY)
        pic_num += 1
        image_list.append(FileInfo(pic_num, file_path, count_white_px2(src)))

        # cv.imshow("tmp", src)
        # cv.imshow("img", img)
        # cv.imshow("cl1", cl1)
        # cv.waitKey()
    return pic_num


def main():
    regex = r"\d\d\d\d-\d\d-\d\d \d\d-\d\d-\d\d"
    crop_list = {
        "Brama5_K13": [[0, 0], [0, 1080], [560, 1080], [310, 240],[960, 190], [1720, 440], [710, 1080], [1920, 1080], [1920, 0]],
        "Brama3_K21": [[0, 0], [0, 720], [1280, 720], [340, 310], [640, 240], [1190, 410], [980, 590], [1280, 720], [1280, 0]],
        "E-4_K03": [[0, 0], [0, 660], [490, 160], [840, 260], [750, 720], [1280, 720], [1280, 0]]
    }
    # [Load image]

    directory = os.getcwd() + "/img"
    glob_path = Path(directory)
    file_list = [str(pp) for pp in glob_path.glob("**/*.Png")]

    pic_num = 0
    for file_path in file_list:  # assuming gif
        img = cv.imread(file_path, cv.IMREAD_GRAYSCALE)
        filename = os.path.split(file_path)[1]
        date = re.search(regex, filename).group()
        name = filename.split('_')
        if len(name) > 2 and name[0]+'_'+name[1] in crop_list:
            cv.fillPoly(img, [crop_road(crop_list[name[0]+'_'+name[1]])], (0, 0, 0))

        time = datetime.datetime.strptime(date, '%Y-%m-%d %H-%M-%S').time()
        pic_num = slippery(time, filename, img, pic_num)

    write_file(image_list)
    # cv.waitKey()
    return


class FileInfo:

    def __init__(self, pic_num, name, white_px):
        self.pic_num = pic_num
        self.name = name
        self.white_px_diff = white_px

    def set_result(self, result):
        self.result = result

    def __str__(self):
        return "{},\tw_px= {},\tratio= {:.3f},\tavg= {:.3f},\tstddev= {:.3f}".format(
            #self.pic_num,
            self.name,
            #self.result,
            sum(self.white_px_diff),
            (len(self.white_px_diff) - self.white_px_diff.count(0)) / len(self.white_px_diff),
            np.mean(self.white_px_diff),
            np.std(self.white_px_diff)
        )

    pic_num = 0
    name = ""
    white_px_diff = 0
    result = ""


###################################################################################################
if __name__ == "__main__":
    main()
