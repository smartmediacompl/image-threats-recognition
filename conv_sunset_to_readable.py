from __future__ import print_function
from __future__ import division
from skimage.measure import compare_ssim
from PIL import Image
import cv2 as cv
import numpy as np
import argparse
import os
from pathlib import Path
import re
import datetime


###################################################################################################
sunset_list = []


###################################################################################################
def write_file(filename):
    f_name = open('r_'+filename, "w+")
    for sunset in sunset_list:
        f_name.write("{}\r".format(sunset))

    f_name.close()
    return


def get_sunset_time(line):
    regex = r"\d\d:\d\d"
    data = line.split("	")

    if len(data) < 3:
        return Sunset
    s_time = re.search(regex, data[1]).group()
    t_time = re.search(regex, data[2]).group()

    return Sunset(
        data[0],
        datetime.datetime.strptime(s_time, '%H:%M').time(),
        datetime.datetime.strptime(t_time, '%H:%M').time())


def read_file(content):
    mount_counter = 1
    counter = 0
    for line in content:
        sunset = get_sunset_time(line)
        if sunset is None:
            return
        lp = int(sunset.day_num)
        if counter < lp:
            counter = lp
            sunset.set_month(mount_counter)
        else:
            counter = 0
            mount_counter = mount_counter + 1
            if mount_counter > 12:
                return
            sunset.set_month(mount_counter)
        sunset_list.append(sunset)
    return


def print_sunset_list():
    for data in sunset_list:
        print(data)
    return


def main():
    directory = os.getcwd()
    glob_path = Path(directory)
    file_list = [str(pp) for pp in glob_path.glob("Sunset*.txt")]

    for file_path in file_list:  # assuming gif
        filename = os.path.split(file_path)[1]
        f = open(filename, "r")
        content = f.readlines()
        read_file(content)
        print_sunset_list()
        write_file(filename)

    return


###################################################################################################
class Sunset:

    def __init__(self, day_num, sunrise, thesaurus):
        self.day_num = day_num
        self.sunrise = sunrise
        self.thesaurus = thesaurus

    def set_month(self, month):
        self.month = month

    def __str__(self):
        return "{}-{};{};{}".format(self.day_num, self.month, self.sunrise, self.thesaurus)

    day_num = 0
    sunrise = datetime.datetime.strptime('00:00:00', '%H:%M:%S').time()
    thesaurus = datetime.datetime.strptime('00:00:00', '%H:%M:%S').time()
    month = 0


###################################################################################################
if __name__ == "__main__":
    main()
